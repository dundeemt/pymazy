'''pymazy classes, base classes and resources'''

from collections import namedtuple
import yaml


class Maze:
    '''represents an unalterable Maze, used to make sense out of a .map file
    and handle maze specific information.'''
    def __init__(self, map_yaml):
        mdata = yaml.load(map_yaml)
        self._name = mdata['name']
        mapl = mdata['map'].split('\n')
        self._layout = ((),)  # tuple of tuples, width x height
        temp_layout = []

        while mapl:
            temp_layout.append(tuple([c for c in mapl.pop(0).strip()]))
        self._layout = tuple(temp_layout)
        self._height = len(self._layout)
        self._width = len(self.layout[0])

        yrow = 0
        for row in self._layout:
            try:
                self._start = Coordinates(row.index('s'), yrow)
            except ValueError:
                pass

            try:
                self._end = Coordinates(row.index('e'), yrow)
            except ValueError:
                pass
            yrow += 1

    @property
    def name(self):
        '''returns the name of the maze'''
        return self._name

    @property
    def height(self):
        '''returns the height of the maze'''
        return self._height

    @property
    def width(self):
        '''returns the width of the maze'''
        return self._width

    @property
    def layout(self):
        '''returns the maze'''
        return self._layout

    @property
    def start_location(self):
        '''return the starting location coordinates'''
        return self._start

    @property
    def end_location(self):
        '''return the ending location coordinates'''
        return self._end


class Game:
    '''handles interaction between strategy and the maze, keeps stats, a map,
    etc'''
    def __init__(self, maze, strategy):
        '''A game is based on a Maze object and a strategy'''
        #self.maze = maze
        self.strategy = strategy
        self.num_moves = 0
        self._status = 'ongoing'
        self.map = MapGame(maze)
        startpt = maze.start_location
        self.loc_x = startpt.x
        self.loc_y = startpt.y
        self._move = ''
        # 4 * sqrt(width^2 + height^2) then round up to nearest integer
        self._max_moves = int(
            round(4 * (maze.width ** 2 + maze.height ** 2) ** .5))

    def current_map(self):
        '''return the current state of the game map'''
        pass

    def current_stats(self):
        '''return the current statistics, number of moves, etc'''
        return self.num_moves

    def next_move(self):
        '''progress the game by having strategy perform it's next move'''

        self._move = self.strategy.move(self.observe)
        print "strat-move:", self._move
        new_loc = self.map.calculate_new_position(self._move)
        print new_loc
        north, east, south, west, origin = self.map.recon_location(new_loc.x,
                                                                    new_loc.y)
        #if new origin/location legal (not out of bounds, on a wall, game won,
        #etc) then record the new location of the player.  Otherwise update
        #game-status
        print "ori:", origin
        if origin == 'e':
            self._status = "Win"
        elif origin == '*':
            self._status = "Lose"

        self.map.record(Observation(self.num_moves, new_loc.x, new_loc.y,
                                    north, east, south, west))
        self.num_moves += 1
        self.loc_x = new_loc.x
        self.loc_y = new_loc.y
        #self.map.current()

    @property
    def observe(self):
        '''using the players current x, y create and return an observation'''
        north, east, south, west, _ori = self.map.recon_location(self.loc_x,
                                                                 self.loc_y)
        return Observation(count=self.num_moves,
                           x=self.loc_x, y=self.loc_y,
                           N=north, E=east, S=south, W=west)

    @property
    def status(self):
        '''return the game status, Won/Lost/Ongoing '''
        #you could loose by hitting a wall, taking too many turns, etc
        #you win by moving to the 'e'xit with legal moves
        #return 'ongoing' if not 'Lost' or 'Won'
        return self._status


class Solver(object):
    '''players will implement their strategies by creating a subclass of the
    solver base class'''

    def __init__(self, width, height, start_coords):
        '''set up a map, so we can show it to the user'''
        self.map = MapPlayer(width, height, start_coords)

    def move(self, observation):
        '''determine next move and return it. This method must be overridden
        by the subclass.'''
        raise NotImplementedError

    def game_over(self, observation):
        '''a chance to review what happened, maybe log results'''
        pass


class Map(object):
    '''base map class, used by MapGame and MapPlayer'''

    def __init__(self, layout=None, start_coords=None, map_name=''):
        '''layout is a list of lists representing what you know about the maze
        start_coords is a Coordinates tuple with the starting x,y'''
        self._width = len(layout[0])
        self._height = len(layout)
        self.layout = layout
        self._map_name = map_name

        self._moves = []
        self._start_coords = start_coords
        #self.current()

    @property
    def current_coords(self):
        '''return the current coordinates of the player'''
        try:
            last_obs = self._moves[-1]
        except IndexError:
            last_obs = self._start_coords

        return Coordinates(last_obs.x, last_obs.y)

    def record(self, observation):
        '''record the current observation, update the map based on it'''
        cur = self.current_coords
        self._moves.append(observation)

        #update current pos marker from @ -> 0
        self.layout[cur.y][cur.x] = 'o'
        self.layout[observation.y][observation.x] = '@'

        #update map based on NESW information
        if observation.y > 0:
            self.layout[observation.y - 1][observation.x] = observation.N
        if observation.y < self._height - 1:
            self.layout[observation.y + 1][observation.x] = observation.S
        if observation.x > 0:
            self.layout[observation.y][observation.x - 1] = observation.W
        if observation.x < self._width - 1:
            self.layout[observation.y][observation.x + 1] = observation.E


    def current(self):
        '''convenience method to print out the current map state'''
        print "+-- %s Map --+" % self._map_name
        for row in self.layout:
            print row


class MapGame(Map):
    '''a Game map object, is like a Maze object, except that it is Modifiable.
    The game object uses it to keep state and history for a game.'''

    def __init__(self, maze):
        self._maze = maze
        layout = [list(row) for row in maze.layout]    # cnvt to list

        super(MapGame, self).__init__(layout=layout,
              start_coords=maze.start_location, map_name='Game')


    def recon_location(self, locx, locy):
        '''return the observations for coordinates passed as if moved there'''
        #self.layout[y][x] n=y-1, s=y+1, e=x+1, w=x-1
        north = '`'
        east = '`'
        south = '`'
        west = '`'
        try:
            last_obs = self._moves[-1]
        except IndexError:
            last_obs = None
        if last_obs:
            self.layout[last_obs.y][last_obs.x] = 'o'

        if locy > 0:
            north = self.layout[locy - 1][locx]
        if locy < len(self.layout) - 1:
            south = self.layout[locy + 1][locx]
        if locx < len(self.layout[0]) - 1:
            east = self.layout[locy][locx + 1]
        if locx > 0:
            west = self.layout[locy][locx - 1]
        origin = self.layout[locy][locx]

        if last_obs:
            self.layout[last_obs.y][last_obs.x] = '@'

        return north, east, south, west, origin

    def calculate_new_position(self, direction):
        '''calculate new coordinates based on current location and navigation
        request'''
        cur = self.current_coords
        if direction == 'N':
            new_position = Coordinates(cur.x,
                                       cur.y - 1)
        elif direction == 'S':
            new_position = Coordinates(cur.x,
                                       cur.y + 1)
        elif direction == 'E':
            new_position = Coordinates(cur.x + 1,
                                       cur.y)
        elif direction == 'W':
            new_position = Coordinates(cur.x - 1,
                                       cur.y)
        else:
            raise ValueError('Unknown navigation request: %s' % direction)
        return new_position


class MapPlayer(Map):
    '''a Player Map object, is like a Maze object, except that it belongs to the
    strategy and you can modify it. Used by the strategy/solver subclasses'''

    def __init__(self, width=0, height=0, start_coords=None):
        #build an empty map [['', '', ...]] of 0-len strings
        row = []
        for _ in range(width):
            row.append('.')
        layout = []
        for _ in range(height):
            layout.append(row[:])

        super(MapPlayer, self).__init__(layout=layout,
              start_coords=start_coords, map_name='Player')


# the current state of the player, passed from game to Stratgey
# pylint: disable-msg=C0103
Observation = namedtuple('Observation',
                         ['count', 'x', 'y', 'N', 'E', 'S', 'W'])

Coordinates = namedtuple('Coordinates', ['x', 'y'])


def import_player_strategy(sbuf):
    'sbuf is in form module.class, returns an instance of the class'
    lbuf = sbuf.split('.')
    class_name = lbuf.pop()
    module_name = '.'.join(lbuf)
    tmod = __import__(module_name)
    return getattr(tmod, class_name)