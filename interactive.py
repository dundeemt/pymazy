'''play an interactive game through the maze, flesh out the Game class'''
import sys

from pymazy import Solver


class InteractiveStrategy(Solver):
    '''an interactive strategy'''

    def move(self, observation):
        '''return a direction to move N/E/S/W'''
        print observation
        self.map.record(observation)
        self.map.current()
        rsp = ' '
        while rsp not in 'neswq':
            rsp = raw_input("Direction (n/e/s/w or (q)uit: ")
            if not rsp:
                rsp = ' '

        if rsp == 'q':
            print "(q)uitting by request"
            sys.exit()
        else:
            return rsp.upper()
