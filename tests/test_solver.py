'''simple tests for the base solver class'''
import sys, os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../')

from pymazy import Solver, Observation, Coordinates

import pytest


class ESNW(Solver):
    '''prefer to move right, then down'''

    def move(self, observation):
        '''return a move to the game based on the strategy.
        observation .count, .x, .y, .N, .E, .S, .W'''
        directions = ('E', 'S', 'N', 'W')
        obs = (observation.E, observation.S, observation.N, observation.W)
        try:
            return directions[obs.index('e')]
        except ValueError:
            return directions[obs.index(' ')]


def test_required_override_next():
    '''require override of .next method'''
    class BSolver(Solver):
        '''sub class w/o overridding .next'''
        pass
    bad_solver = BSolver(2, 2, Coordinates(0, 2))
    obs = Observation(0, 0, 2, '*', ' ', '*', '*')

    with pytest.raises(NotImplementedError):
        assert 'E' == bad_solver.move(obs)


def test_simple_move():
    '''test a simple interaction passing an obs, and return a direction'''
    esnw = ESNW(6, 6, Coordinates(0, 2))
    obs = Observation(0, 0, 2, '*', ' ', '*', '*')
    assert 'E' == esnw.move(obs)


def test_traverse():
    '''test a traverse of the maze'''
    esnw = ESNW(6, 6, Coordinates(0, 2))
    obs = Observation(0, 0, 2, '*', ' ', '*', '*')
    for i in range(9):
        assert 'E' == esnw.move(obs)
        obs = Observation(i, 1+i, 2, ' ', ' ', ' ', 'o')

    obs = Observation(9, 9, 2, ' ', '*', ' ', 'o')
    assert 'S' == esnw.move(obs)

    obs = Observation(10, 9, 3, 'o', '*', ' ', ' ')
    assert 'S' == esnw.move(obs)

    obs = Observation(11, 9, 4, 'o', 'e', ' ', ' ')
    assert 'E' == esnw.move(obs)
