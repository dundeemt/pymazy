'''test maze class'''

import sys, os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../')

from pymazy import Maze, Coordinates

MAZE_DATA = file('maps/001.map', 'rb').read()
MAZE = Maze(MAZE_DATA)

def test_maze_name():
    '''test reading the maze name from the data'''
    assert MAZE.name == 'map one'

def test_maze_height():
    '''test reading map height'''
    assert MAZE.height == 10

def test_maze_width():
    '''test reading map height'''
    assert MAZE.width == 10

def test_maze_layout():
    '''test reading map layout'''
    assert MAZE.layout == (
                            ('*', '*', '*', '*', '*', '*', '*', '*', '*', '*'),
                            ('*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'),
                            ('s', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'),
                            ('*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'),
                            ('*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'),
                            ('*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'),
                            ('*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'),
                            ('*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'e'),
                            ('*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'),
                            ('*', '*', '*', '*', '*', '*', '*', '*', '*', '*'),
                          )

def test_maze_start_loc():
    '''test the start location detection'''
    assert MAZE.start_location == Coordinates(0, 2)

def test_maze_end_loc():
    '''test the start location detection'''
    assert MAZE.end_location == Coordinates(9, 7)