'''test map class, should initialize with a Maze, then update map based
on play.  Map should have a method to interrogate the state of the map so
it can be visualized by a player.  A blind map is a list of lists of empty
strings.  location information/observations as recorded as encountered. A
blind map requires height/width information and no maze.layout data'''

import sys, os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../')

from pymazy import Maze, MapPlayer, MapGame, Observation, Coordinates

MAZE_DATA = file('maps/001.map', 'rb').read()
MAZE = Maze(MAZE_DATA)


def test_gamemap_init():
    '''verify that the initial layouts match up'''
    mzmap = MapGame(MAZE)
    assert MAZE.layout == tuple(tuple(row) for row in mzmap.layout)


def test_playermap_init():
    '''verify that the initial player layouts match up'''
    mzmap = MapPlayer(width=MAZE.width, height=MAZE.height)
    assert mzmap.layout == [
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.']
                           ]


def test_map_start():
    '''test a first move map'''
    mzmap = MapGame(MAZE)
    obs = Observation(0, 0, 2, '*', ' ', '*', '*')
    mzmap.record(obs)
    assert mzmap.layout == [
                            ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['@', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'e'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*']
                           ]


def test_map_move_1():
    '''test move number 1'''
    mzmap = MapGame(MAZE)
    obs = Observation(0, 0, 2, '*', ' ', '*', '*')
    mzmap.record(obs)
    obs = Observation(1, 1, 2, ' ', ' ', ' ', 'o')
    mzmap.record(obs)
    assert mzmap.layout == [
                            ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['o', '@', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'e'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*']
                           ]


def test_map_move_2():
    '''test move number 2'''
    mzmap = MapGame(MAZE)
    obs = Observation(0, 0, 2, '*', ' ', '*', '*')
    mzmap.record(obs)
    obs = Observation(1, 1, 2, ' ', ' ', ' ', 'o')
    mzmap.record(obs)
    obs = Observation(2, 1, 3, 'o', ' ', ' ', '*')
    mzmap.record(obs)
    assert mzmap.layout == [
                            ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['o', 'o', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', '@', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'e'],
                            ['*', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '*'],
                            ['*', '*', '*', '*', '*', '*', '*', '*', '*', '*']
                           ]


def test_blind_map_move_2():
    '''test a blind map move number 2, 0-length string is an unknown/blind'''
    mzmap = MapPlayer(width=10, height=10, start_coords=Coordinates(0, 2))
    #mzmap.current()
    #print 10*"+"
    obs = Observation(0, 0, 2, '*', ' ', '*', '*')
    mzmap.record(obs)
    #mzmap.current()
    #print 10*"+"
    obs = Observation(1, 1, 2, ' ', ' ', ' ', 'o')
    mzmap.record(obs)
    #mzmap.current()
    #print 10*"+"
    obs = Observation(2, 1, 3, 'o', ' ', ' ', '*')
    mzmap.record(obs)
    #mzmap.current()
    assert mzmap.layout == [
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['*', ' ', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['o', 'o', ' ', '.', '.', '.', '.', '.', '.', '.'],
                            ['*', '@', ' ', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', ' ', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.'],
                            ['.', '.', '.', '.', '.', '.', '.', '.', '.', '.']
                           ]

