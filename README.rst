======
pymazy
======

Goal: A game based environment to learn/teach programming and AI problem solving.

To use, you should create a module with a class that Subclasses mazy.Solver
That class should override the .move() method.  The .move method is where you
build your maze solving code.  

  mazy.py interactive.InteractiveStrategy

Notes
=====

 observation is a NamedTuple in the form:
   **Observation(count=0, x=0, y=2, N='*', E=' ', S='*', W='`')**

   where:

     **count** = (int)the number of moves made by the strategy to this point in the game

     **x** = (int)strategy's current x position in the maze as a grid top left is 0,0

     **y** = (int)strategy's current y position in the maze as a grid top left is 0,0

     **N** = (char)observed attribute of the position directly North(above) the current location

     **E** = (char)observed attribute of the position directly East(right) the current location

     **S** = (char)observed attribute of the position directly South(below) the current location

     **W** = (char)observed attribute of the position directly West(left) the current location

   possible observed attributes:
    
    **' '** = Open
    
    **'o'** = already visited
    
    **'*'** = Wall
    
    **'s'** = start location
    
    **'e'** = end location
    
    **'`'** = Off the map, illegal



http://rst.ninjs.org/
