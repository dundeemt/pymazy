'''play a maze game through the maze, flesh out the Game class'''

import glob
import sys

from pymazy import Game, Maze, import_player_strategy


def play_game(klass):
    '''play an interactive maze'''
    for maze_file in glob.glob('maps/*.map'):
        maze_data = file(maze_file, 'rb').read()
        maze = Maze(maze_data)
        strategy = klass(maze.width, maze.height, maze.start_location)
        game = Game(maze, strategy)
        print 'game.status:', game.status
        while game.status == 'ongoing':
            game.next_move()
            print game.status
        print game.status


if __name__ == '__main__':

    print sys.argv[1]
    STRATEGY_KLASS = import_player_strategy(sys.argv[1])

    play_game(STRATEGY_KLASS)
